package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
)

const DefaultConfigFile = "dodo.yml"

type Command []string

func (c *Command) Set(value string) error {
	*c = append(*c, value)
	return nil
}

func (c *Command) String() string {
	return ""
}

func (c *Command) IsCumulative() bool {
	return true
}

type Args struct {
	Command    *Command
	ConfigFile *string
}

func getArgs() (args Args) {
	commandArg := kingpin.Arg("Command", "The Command(s) to execute").Required()
	args = Args{
		Command:    commandList(commandArg),
		ConfigFile: kingpin.Flag("file", "The config file to user (defaults to dodo.yml)").Default(DefaultConfigFile).Short('f').String(),
	}
	kingpin.Parse()
	return
}

func commandList(a *kingpin.ArgClause) (target *Command) {
	target = new(Command)
	a.SetValue(target)
	return
}
