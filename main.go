package main

import (
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
)

func main() {

	args := getArgs()

	conf := getConfig(args)

	if conf.ImageIsSpecified() {
		runContainer(conf.Image, *args.Command)
		os.Exit(0)
	}

	if conf.ComposeIsSpecified() {
		image := conf.Compose.GetImage()
		runContainer(image, *args.Command)
		os.Exit(0)
	}

	log.Fatalln("Neither a docker image nor a docker-compose service is specified in your configuration")
}

func (compose *Compose) GetImage() string {
	// Read Compose File
	content, err := ioutil.ReadFile(compose.File)
	if err != nil {
		log.Fatalln("Error while reading file: ", err)
	}

	composeConf := &ComposeConf{}
	err = yaml.Unmarshal([]byte(content), &composeConf)
	if err != nil {
		log.Fatalln("Error while parsing compose configuration: ", err)
	}

	return composeConf.Services[compose.Service].Image
}

func (conf *Conf) ComposeIsSpecified() bool {
	return conf.Compose.File != "" && conf.Compose.Service != ""
}

func (conf *Conf) ImageIsSpecified() bool {
	return conf.Image != ""
}

func runContainer(image string, cmd Command) {
	ctx := context.Background()
	cli, err := client.NewClientWithOpts()
	if err != nil {
		panic(err)
	}

	_, err = cli.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		panic(err)
	}

	inspect, _, err := cli.ImageInspectWithRaw(ctx, image)
	if err != nil {
		log.Fatalln(err)
	}

	workingDir := inspect.Config.WorkingDir

	if workingDir == "" {
		workingDir = "/dodo"
	}

	containerConfig := &container.Config{
		Image: image,
		Cmd:   []string(cmd),
		Tty:   true,
	}

	containerHostConfig := &container.HostConfig{
		Binds: []string{
			pwd() + ":" + workingDir,
		},
	}

	containerName := "dodo_" + time.Now().Format("2006-01-02_15-04-05")

	resp, err := cli.ContainerCreate(ctx, containerConfig, containerHostConfig, nil, containerName)
	if err != nil {
		panic(err)
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

	statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			panic(err)
		}
	case <-statusCh:
	}

	out, err := cli.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{ShowStdout: true})
	if err != nil {
		panic(err)
	}

	_, err = io.Copy(os.Stdout, out)
	if err != nil {
		log.Fatalln(err)
	}

	err = cli.ContainerRemove(ctx, resp.ID, types.ContainerRemoveOptions{})
	if err != nil {
		log.Fatalln(err)
	}
}

func pwd() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatalln(err)
	}

	return dir
}

type Service struct {
	Image string
}

type Services map[string]Service

type ComposeConf struct {
	Services Services
}

type Compose struct {
	File    string
	Service string
}

type Conf struct {
	Image   string
	Compose Compose
}

func getConfig(args Args) Conf {

	configFilePath := getConfigFile(args)

	// Read File
	content, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		log.Fatalln("Error while reading file: ", err)
	}

	conf := Conf{}

	err = yaml.Unmarshal([]byte(content), &conf)
	if err != nil {
		log.Fatalln("Error while parsing configuration: ", err)
	}

	return conf
}

func getConfigFile(args Args) string {

	// First check if local configuration file exists
	if fileExists(*args.ConfigFile) {
		return *args.ConfigFile
	}

	// Get user configuration file
	usr, err := user.Current()
	if err != nil {
		log.Fatalln(err)
	}
	userConfFilePath := usr.HomeDir + "/.config/dodo.yml"

	// Then check if user configuration file exists
	if fileExists(userConfFilePath) {
		return userConfFilePath
	}

	// If nothing exists output error message
	log.Fatalln("No existing configuration files were found. Please create a user or local config and make sure your spelling is correct")
	return ""
}

func fileExists(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil
}
